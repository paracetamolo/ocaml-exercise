#! /bin/sh

set -e

ocaml_version=4.12.0
opam_version=2.0
ocamlformat_version=0.18.0
rust_version=1.44.0

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"

if [ ! -d "$src_dir/_opam" ] ; then

    if [ ! -x "$(command -v cargo)" ] ; then
        curl --proto '=https' --tlsv1.2 https://sh.rustup.rs/rustup-init.sh -o rustup-init.sh
        chmod +x rustup-init.sh
        ./rustup-init.sh --profile minimal --default-toolchain $rust_version -y
        rm rustup-init.sh
    fi
    . $HOME/.cargo/env

    export OPAMYES=${OPAMYES:=true}
    opam repository set-url default https://opam.ocaml.org
    opam update
    opam switch create "$src_dir" ocaml-base-compiler.$ocaml_version --no-install
    opam pin ocamlformat $ocamlformat_version
    opam install opam-depext odoc merlin
    opam depext conf-gmp conf-pkg-config conf-libffi
    opam install . --deps-only --with-test
fi
eval $(opam env --shell=sh)
